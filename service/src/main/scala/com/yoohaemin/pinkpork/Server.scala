package com.yoohaemin.pinkpork

import cats.syntax.all._
import cats.effect._
import fs2.StreamApp.ExitCode
import fs2.{ Stream, StreamApp }
import org.http4s.server.blaze.BlazeBuilder
import com.yoohaemin.pinkpork.config.Config

import scala.concurrent.ExecutionContext.Implicits.global

object Server extends Main[IO]

abstract class Main[F[_]: ConcurrentEffect] extends StreamApp[F] {

  override def stream(args: List[String], requestShutdown: F[Unit]): Stream[F, ExitCode] =
    for {
      config <- Config.stream[F]
      ctx = new Module[F](config)
      exitCode <- BlazeBuilder[F]
        .bindHttp()
        .mountService(ctx.userHttpEndpoint, "/users")
        .serve
    } yield exitCode

}
