package com.yoohaemin.pinkpork.config

import cats.effect.Sync
import fs2.Stream
import pureconfig._
import pureconfig.module.catseffect._

object Config {
  case class DBUrl(value: String) extends AnyVal
  case class DBUsername(value: String) extends AnyVal
  case class DBPassword(value: String) extends AnyVal

  case class DBConfig(url: DBUrl, username: DBUsername, password: DBPassword)

  case class UserConfig()

  case class AppConfig(db: DBConfig, user: UserConfig)

  def load[F[_]: Sync]: F[AppConfig] = loadConfigF[F, AppConfig]
  def stream[F[_]: Sync]: Stream[F, AppConfig] = Stream.eval(load)
}
