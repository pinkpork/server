package com.yoohaemin.pinkpork.repository

import com.yoohaemin.pinkpork.model.{User, UserName}

object algebra {

  trait UserRepository[F[_]] {
    def findUser(username: UserName): F[Option[User]]
  }

}
