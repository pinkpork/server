package com.yoohaemin.pinkpork

import cats.effect._
import com.yoohaemin.pinkpork.config.Config.AppConfig
import com.yoohaemin.pinkpork.http.{HttpErrorHandler, UserHttpEndpoint}
import com.yoohaemin.pinkpork.repository.MysqlUserRepository
import com.yoohaemin.pinkpork.repository.algebra.UserRepository
import com.yoohaemin.pinkpork.service.UserService
import doobie.util.transactor.Transactor
import org.http4s.HttpRoutes

class Module[F[_] : Async](config: AppConfig) {

  private val xa: Transactor[F] =
    Transactor.fromDriverManager[F](
      "com.mysql.jdbc.Driver", config.db.url.value, config.db.username.value, config.db.password.value
    )

  private val userRepository: UserRepository[F] = new MysqlUserRepository[F](xa)

  private val userService: UserService[F] = new UserService[F](userRepository)

  implicit val httpErrorHandler: HttpErrorHandler[F] = new HttpErrorHandler[F]

  val userHttpEndpoint: HttpRoutes[F] = new UserHttpEndpoint[F](userService).routes

}
