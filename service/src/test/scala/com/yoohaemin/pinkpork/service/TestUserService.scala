package com.yoohaemin.pinkpork.service

import cats.effect.IO
import com.yoohaemin.pinkpork.TestUsers.users
import com.yoohaemin.pinkpork.model.UserName
import com.yoohaemin.pinkpork.repository.algebra.UserRepository

object TestUserService {

  private val testUserRepo: UserRepository[IO] =
    (username: UserName) => IO {
      users.find(_.username.value == username.value)
    }

  val service: UserService[IO] = new UserService[IO](testUserRepo)

}
