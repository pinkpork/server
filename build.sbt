
// projects
lazy val root = project
  .in(file("."))
  .settings(settings)
  .aggregate(service, client)

lazy val service = project
  .settings(
    name := "pinkpork-service",
    settings,
    // assemblySettings,
    libraryDependencies ++= commonDependencies ++
      Nil
  )

lazy val client = project
  .settings(
    settings,
    name := "pinkpork-client",
    libraryDependencies ++= commonDependencies ++
      Nil
  )

// DEPENDENCIES

lazy val D = new {
  val V = new {
    val http4s     = "0.19.0-M1"
    val circe      = "0.10.0-M2"
    val doobie     = "0.6.0-M2"
    val pureConfig = "0.9.2"
    val mysql      = "8.0.12"
    val logback    = "1.2.3"
    val scalaTest  = "3.0.3"
    val scalaCheck = "1.13.4"
  }

  def http4s(artifact: String) = "org.http4s"   %% s"http4s-$artifact" % V.http4s
  def circe(artifact: String)  = "io.circe"     %% s"circe-$artifact"  % V.circe
  def doobie(artifact: String) = "org.tpolecat" %% s"doobie-$artifact" % V.doobie
  def pureConfig(artifact: String) =
    "com.github.pureconfig" %% s"pureconfig${artifact.headOption.fold("")(_ => "-")}$artifact" % V.pureConfig

  val http4sBlaze          = http4s("blaze-server")
  val http4sCirce          = http4s("circe")
  val http4sDsl            = http4s("dsl")
  val circeCore            = circe("core")
  val circeGeneric         = circe("generic")
  val doobieCore           = doobie("core")
  val doobieHikari         = doobie("hikari")
  val doobieScalatest      = doobie("scalatest")
  val pureConfigCore       = pureConfig("")
  val pureConfigCats       = pureConfig("cats")
  val pureConfigCatsEffect = pureConfig("cats-effect")
  val mysql                = "mysql"                 %  "mysql-connector-java" % V.mysql
  val logback              = "ch.qos.logback"        %  "logback-classic"      % V.logback
  val scalaTest            = "org.scalatest"         %% "scalatest"            % V.scalaTest  % Test
  val scalaCheck           = "org.scalacheck"        %% "scalacheck"           % V.scalaCheck % Test
}

lazy val commonDependencies = {
  import D._

  http4sBlaze     ::
    http4sCirce          ::
    http4sDsl            ::
    circeCore            ::
    circeGeneric         ::
    doobieCore           ::
    doobieHikari         ::
    doobieScalatest      ::
    pureConfigCore       ::
    pureConfigCats       ::
    pureConfigCatsEffect ::
    mysql                ::
    logback              ::
    scalaTest            ::
    scalaCheck           ::
    Nil
}

// SETTINGS

lazy val settings = commonSettings

lazy val scalacOpt = Seq(
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-Ypartial-unification",
  "-deprecation",
  "-encoding",
  "utf8"
)

lazy val commonSettings = Seq(
  scalacOptions ++= scalacOpt,
  organization := "com.yoohaemin",
  version := "0.0.1-SNAPSHOT",
  scalaVersion in ThisBuild := "2.11.12",
  resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )
)

/*
lazy val assemblySettings = Seq(
  assemblyJarName in assembly := name.value + ".jar",
  assemblyMergeStrategy in assembly := {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case _                             => MergeStrategy.first
  }
)
 */
